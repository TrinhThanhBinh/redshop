<?php

namespace Page;

class CategoryPage
{
    public static $urlCategoryPage = "administrator/index.php?option=com_redshop&view=categories";
    /**
     * @var string
     */
    public static $categoryMenuLink = "//a[@class='active']";

    /**
     * @var string
     */
    public static $newButton = ".button-new";

    /**
     * @var string
     */
    public static $deleteButton = ".button-delete";

    /**
     * @var string
     */
    public static $saveAndCloseButton = ".button-save";

    /**
     * @var string
     */
    public static $saveAndNewButton = ".button-save-new";

    /**
     * @var string
     */
    public static $categoryName = "#jform_name";

    /**
     * @var string
     */
    public static $categoryParentDropdown = "#s2id_jform_parent_id";

    /**
     * @var string
     */
    public static $publicRadioButton = "#jform_published0";

    /**
     * @var string
     */
    public static $unPublicRadioButton = "#jform_published1";

    /**
     * @var string
     */
    public static $numOfProduct = "#jform_products_per_page";

    /**
     * @var string
     */
    public static $templateDropdown = "//div[@id='s2id_jform_template']//a";

    /**
     * @var string
     */
    public static $templateCategoryParents = "//div[contains(text(),'- Templates')]";

    /**
     * @var string
     */
    public static $allowTemplateSelect = "#s2id_jform_more_template";

    /**
     * @var string
     */
    public static $listTemplate = "//div[contains(text(),'list')]";

    /**
     * @var string
     */
    public static $listSelect = "//option[contains(text(),'list')]";

    /**
     * @var string
     */
    public static $productComparisionTemplate = "//div[@id='s2id_jform_compare_template_id']//a";

    /**
     * @var string
     */
    public static $compareProductTemplate = "//div[contains(text(),'compare_product')]";

    /**
     * @var string
     */
    public static $backImage = "#category_back_full_image-dropzone";

    /**
     * @var string
     */
    public static $categoryImage = "#category_full_image-dropzone";

    /**
     * @var string
     */
    public static $createBy = "jform_created_by";

    /**
     * @var string
     */
    public static $toggleEditorButton = "//div[8]//div[1]//div[1]//div[2]//div[1]//a[1]";

    /**
     * @var string
     */
    public static $editArea = "#mceu_160";

    /**
     * @var string
     */
    public static $createSuccessMessage = "Item saved.";

    /**
     * @var string
     */
    public static $searchTemplate = "#s2id_autogen10_search";
}