<?php

class OrderPage
{
    /**
     * @var string
     */
    public static $menuUser = ".menu-user";

    /**
     * @var string
     */
    public static $userTitle = "Users";

    /**
     * @var string
     */
    public static $newButton = ".button-new";

    /**
     * @var string
     */
    public static $nameInput = "#jform_name";

    /**
     * @var string
     */
    public static $userNameInput = "#jform_username";

    /**
     * @var string
     */
    public static $passwordInput = "#jform_password";

    /**
     * @var string
     */
    public static $passwordConfirmInput = "#jform_password2";

    /**
     * @var string
     */
    public static $emailInput = "#jform_email";

    /**
     * @var string
     */
    public static $resetCountLb = "#jform_resetCount-lbl";

    /**
     * @var string
     */
    public static $sendMailYes = "//label[@for='jform_sendEmail0']";

    /**
     * @var string
     */
    public static $jsWindow = 'window.scrollTo(0,0);';

    /**
     * @var string
     */
    public static $assignedUserGroupsTab = "//ul[@id=\"myTabTabs\"]/li[2]/a";

    /**
     * @var string
     */
    public static $basicSettingsTab = "//ul[@id=\"myTabTabs\"]/li[3]";

    /**
     * @var string
     */
    public static $publicGroups = "//input[@id=\"1group_1\"]";

    /**
     * @var string
     */
    public static $backendTemplateStyleId = "#jform_params_admin_style_chzn";

    /**
     * @var string
     */
    public static $hathorDefaultStyle = "//li[contains(text(),'Hathor - Default')]";

    /**
     * @var string
     */
    public static $saveSuccessMessage = "User saved.";

    /**
     * @var string
     */
    public static $searchField = "#filter_search";

    /**
     * @var string
     */
    public static $deleteButton ="//div[@id='toolbar-delete']//button[1]";

    /**
     * @var string
     */
    public static $deleteSuccessMessage ="deleted.";

    /**
     * @var string
     */
    public static $accountTab = "//a[contains(text(),'Account Details')]";
}