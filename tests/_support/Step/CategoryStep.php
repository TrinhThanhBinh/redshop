<?php

namespace Step;
use Page\CategoryPage;
class CategoryStep extends \AcceptanceTester
{
    /**
     * @param $nameCategory
     * @param $number
     * @param $description
     * @throws \Exception
     */
    public function create_Category($nameCategory, $number, $description){
        $I = $this;
        $I->amOnPage(CategoryPage::$urlCategoryPage);
        $I->waitForElementVisible(CategoryPage::$newButton, 30);
        $I->click(CategoryPage::$newButton);
        $I->waitForElementVisible(CategoryPage::$categoryName,30);
        $I->fillField(CategoryPage::$categoryName, $nameCategory);
        $I->scrollTo(CategoryPage::$categoryName);
        $I->waitForElementVisible(CategoryPage::$categoryParentDropdown,30);
        $I->click(CategoryPage::$categoryParentDropdown);
        $I->waitForElementVisible(CategoryPage::$templateCategoryParents,30);
        $I->click(CategoryPage::$templateCategoryParents);
        $I->waitForElementVisible(CategoryPage::$publicRadioButton,30);
        $I->click(CategoryPage::$publicRadioButton);
        $I->waitForElementVisible(CategoryPage::$numOfProduct,30);
        $I->fillField(CategoryPage::$numOfProduct, $number);
//        $I->waitForElementVisible(CategoryPage::$templateDropdown,30);
//        $I->click(CategoryPage::$templateDropdown);
//        $I->waitForElementVisible(CategoryPage::$searchTemplate,30);
//        $I->fillField(CategoryPage::$searchTemplate, $searchTitle);
//        $I->waitForElementVisible(CategoryPage::$listTemplate,30);
//        $I->click(CategoryPage::$listTemplate);
//        $I->waitForElementVisible(CategoryPage::$allowTemplateSelect,30);
//        $I->click(CategoryPage::$allowTemplateSelect);
//        $I->waitForElementVisible(CategoryPage::$listSelect,30);
//        $I->click(CategoryPage::$listSelect);
//        $I->waitForElementVisible(CategoryPage::$productComparisionTemplate,30);
//        $I->click(CategoryPage::$productComparisionTemplate);
//        $I->waitForElementVisible(CategoryPage::$compareProductTemplate,30);
//        $I->click(CategoryPage::$compareProductTemplate);
        $I->scrollTo(CategoryPage::$toggleEditorButton);
        $I->click(CategoryPage::$toggleEditorButton);
        $I->waitForElementVisible(CategoryPage::$editArea,30);
        $I->fillField(CategoryPage::$editArea, $description);
        $I->click(CategoryPage::$saveAndCloseButton);
        $I->waitForText(CategoryPage::$createSuccessMessage);
    }
}