<?php

namespace Step;

use Page\AbstractPage;
class AbstractStep extends \AcceptanceTester
{
    /**
     * @param $userName
     * @param $passWord
     * @throws \Exception
     */
    public function login($userName, $passWord)
    {
        $I = $this;
        $I->amOnPage(AbstractPage::$ulrAdmin);
        $I->waitForElementVisible(AbstractPage::$userNameField, 30);
        $I->fillField(AbstractPage::$userNameField, $userName);
        $I->fillField(AbstractPage::$passwordField, $passWord);
        $I->waitForElementVisible(AbstractPage::$loginButton, 30);
        $I->click(AbstractPage::$loginButton);
        $I->waitForText(AbstractPage::$controlPanelTxt, 30);
        $I->see(AbstractPage::$controlPanelTxt);
    }
}