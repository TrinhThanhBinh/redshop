<?php

use Step\CategoryStep;
use Step\AbstractStep;
class CategoryCest
{
    /**
     * @var \Faker\Generator
     */
    protected $faker;

    /**
     * @var string
     */
    protected $userNameAdmin;

    /**
     * @var string
     */
    protected $passwordAdmin;

    /**
     * @var string
     */
    protected $categoryName;

    /**
     * @var string
     */
    protected $numberPerPage;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $searchTitle;

    /**
     * CategoryCest constructor.
     */
    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
        $this->userNameAdmin ="admin";
        $this->passwordAdmin ="admin";
        $this->searchTitle ="list";
        $this->categoryName = $this->faker->bothify("Category ??");
        $this->numberPerPage = $this->faker->numberBetween(0,10);
        $this->description = $this->faker->text(100);
    }

    /**
     * @param CategoryStep $I
     * @param AbstractStep $tester
     * @throws Exception
     */
    public function category(CategoryStep $I, AbstractStep $tester){
        $tester->login($this->userNameAdmin, $this->passwordAdmin);
        $I->create_Category($this->categoryName, $this->numberPerPage, $this->description);
    }
}